// dStroke (JavaScript Text Stroke / Outline) by daikra



function dStroke (receivedSize, receivedColor, receivedStrokeRounds) {

	var stroke = "0 0 ";

	for (x=1;x<receivedStrokeRounds;x++) { stroke += receivedSize + " " + receivedColor + ", 0 0 "; }

	stroke = stroke.slice(0, -6);

	return stroke;

}

// Example: dStroke ( "1px", "#fff", "3" );
