# dStroke - JavaScript Text Stroke / Outline



*(README UNDER CONSTRUCTION)*

Lightweight tool that adds text stroke in a uniform way unlike the regular "text-shadow".

It basically consists in adding blur text stroke multiple times until it looks like a clear outline.
